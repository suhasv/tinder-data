// app.js

//HELPER METHODS
function configure_app(parameters){
	var opts = {
		preferredGender = 'male',
		userGender  = 'female',
		lat = 42.3601,
		lng = 71.0589
	};

	parameters.forEach(function(arg){
		unpacked_arg = arg.split(':');
		if (unpacked_arg[0]=='g'){
			switch(unpacked_arg[1]){
				case 'fm':
					opts.userGender = 'female';
					opts.preferredGender = 'male';
					break;
				case 'mm':
					opts.userGender = 'male';
					opts.preferredGender = 'female';
					break;
				case 'ff':
					opts.userGender = 'female';
					opts.preferredGender = 'female';
					break;
				case 'mf':
					opts.userGender = 'male';
					opts.preferredGender = 'male';
					break;
				default:
					console.log("Invalid gender arguments.");
			}
		}
		else if (unpacked_arg[0]=='lat'){
			opts.lat = unpacked_arg[1];
		}
		else if (unpacked_arg[0]=='lng'){
			opts.lng = unpacked_arg[1];
		}
		else {
			console.log("Invalid argument type: " + toString(unpacked_arg[1]))
		}
	});
	return opts;
}

// grab parameters from application call
var parameters = process.argv.slice(2);
var options = configure_app(parameters);

// make request to Tinder for recommendations

// store profiles

// repeat

